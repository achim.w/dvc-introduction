#!/bin/bash

# Create and activate virtual Python environment
python3 -m venv venv
. venv/bin/activate

# Install packages with pip
pip install --upgrade pip wheel
pip install -r requirements.txt

# Install DVC git-hooks
dvc install

