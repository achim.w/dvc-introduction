# DVC Introduction

This repository will be used as a sample for the DVC introduction workshop.

## Requirements

To be able to run the code in this repository you need
- [Python3](https://www.python.org/downloads/),
- venv (should be included since Python 3.6), and
- [Pip](https://pip.pypa.io/en/stable/installing/).

Moreover, I recommend [VSCode](https://code.visualstudio.com/download) as an easy to use IDE.

## Setup

Run the script `setup.sh`.
This will create a virtual Python environment and install the required packages with pip.
Afterwards it will install the git-hooks from DVC to this repository.

If you are on Windows, run the script `transform_to_windows_git_bash.sh` first.
I'm not sure if this works completely fine, but it should transform all scripts to run under Windows with Git Bash.

## Usage

I recommend opening this folder in `vscode`.
If you want to use it from the terminal, make sure you
- activate the Python environment with `source venv/bin/activate` and
- execute all Python scripts from the root folder (not the `src` folder).

### Generating Sample data

Use the script in `src/create_data.py` for generating some sample data.

### Curve Fitting

The settings for the curve fitting are inside the file `params.yaml`.
To conduct curve fitting on one of the data tracks run `dvc repro`.
This will execute the following stages:
- `prepare`
	- Read the data from `data/raw/data.csv`,
	- select the n-th data track (configurable in params.yaml),
	- remove any NaNs from the data, and
	- saves the processed data in `data/processed/data.csv`.
- `fit`
	- Read the data from `data/processed/data.csv`.
	- fit a polynom of n-th degree (configurable in params.yaml), and
	- save the polynomial coefficients and residuals to the folder `results`.
- `plot`
	- Read the data form `data/processed/data.csv` and polynomial coefficients from `results` and
	- generate and save a plot with the input data and the fitted curve to the folder `results`.

### Running experiments

You can modify and use the script `src/queue_experiments.py` to queue multiple experiments.
You can then run these experiments with `dvc run --run-all --jobs <n_cpus>`.
Replace `<n_cpus>` with the number of cores to use for parallel calculation of the jobs.

## License

This project is licensed under the MIT License (see LICENSE).

