#!/usr/bin/env python3
import json
import numpy as np
import os
import yaml

# Load params
params = yaml.safe_load(open("params.yaml"))["fit"]
poly_order = params["poly_order"]

# Load data
[x, y] = np.loadtxt("./data/prepared/data.csv")

# Fit data
pfit = np.polyfit(x, y, poly_order, full=True)

# Save results
os.makedirs("./results/", exist_ok=True)
with open("./results/scores.json", "w") as fid:
    json.dump({"residuals": float(pfit[1])}, fid, indent=4)
with open("./results/polynomial_coefficients.json", "w") as fid:
    json.dump({"polynomial_coefficients": list(pfit[0])}, fid, indent=4)
