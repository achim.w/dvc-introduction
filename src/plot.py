#!/usr/bin/env python3
import csv
import json
from matplotlib import pyplot as plt
import numpy as np
import os

# Load data
[x, y] = np.loadtxt("./data/prepared/data.csv")
with open("./results/polynomial_coefficients.json", "r") as fid:
    pfit = json.load(fid)

# Function for calculating function values for polynomial
y_fit = np.poly1d(pfit["polynomial_coefficients"])

# Save difference between polynomial and real data
with open("./results/deviation.csv", "w") as fid:
    csvwriter = csv.writer(fid, delimiter=",")
    csvwriter.writerow(["x", "deviation"])

    for x_i, y_i in zip(x, y_fit(x)-y):
        csvwriter.writerow([x_i, y_i])

# Generate plot with input data and fitted data
plt.plot(x, y, label="Data")
plt.plot(x, y_fit(x), label="Fit")
plt.xlabel("time in s")
plt.ylabel("amplitude in mm")
plt.legend()

os.makedirs("./results/", exist_ok=True)
plt.savefig("results/fitting.png")
