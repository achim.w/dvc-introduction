#!/bin/usr/env python3
import subprocess


for ii in range(1, 9):

    print("Adding polynom-order", ii, "to experiments")

    subprocess_run = subprocess.run(
        [
            "dvc",
            "exp",
            "run",
            "--queue",
            "--name",
            f"poly_order-{ii}",
            "--set-param",
            "prepare.data_track_number=4",
            "--set-param",
            f"fit.poly_order={ii}",
        ],
        capture_output=True,
        check=True,
    )

    print(subprocess_run.stdout)
    print(subprocess_run.stderr)
