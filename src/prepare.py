#!/usr/bin/env python3
import numpy as np
import os
import yaml

# Load params
params = yaml.safe_load(open("params.yaml"))["prepare"]
data_track_number = params["data_track_number"]

# Load data
raw_data = np.loadtxt("data/raw/data.csv")

# Select data
x = raw_data[0]
y = raw_data[data_track_number]

# Filter out NaNs
ind_nan_x = np.isnan(x)
ind_nan_y = np.isnan(y)
ind_nan = np.logical_or(ind_nan_x, ind_nan_y)
x = x[~ind_nan]
y = y[~ind_nan]

# Save data
os.makedirs("./data/prepared/", exist_ok=True)
np.savetxt("./data/prepared/data.csv", [x, y])
