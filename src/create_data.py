#!/usr/bin/env python3
from matplotlib import pyplot as plt
import numpy as np
import os

np.random.seed(0)

# Generate sample data
x = np.arange(-4, 4, 0.01)
y_poly1 = 100*x - 3 + 5 * np.random.randn(np.size(x))
y_poly2 = 20*x**2 + 1*x - 3 + 5 * np.random.randn(np.size(x))
y_poly3 = -4*x**3 - 2*x**2 + 12*x - 3 + 5 * np.random.randn(np.size(x))
y_poly4 = x**4 - 4*x**3 - 2*x**2 + 12*x - 3 + 5 * np.random.randn(np.size(x))
y_sin = 100 * np.sin(2*np.pi*0.25 * x + 0) + 5 * np.random.randn(np.size(x))

# Save data
os.makedirs("./data/raw", exist_ok=True)
np.savetxt(
    "./data/raw/data.csv",
    [x, y_poly1, y_poly2, y_poly3, y_poly4, y_sin],
    header="x, poly1, poly2, poly3, poly4, sin",
)

# Evaluate data graphically
plt.plot(x, y_poly1)
plt.plot(x, y_poly2)
plt.plot(x, y_poly3)
plt.plot(x, y_poly4)
plt.plot(x, y_sin)
plt.show()
