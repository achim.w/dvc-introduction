#!/bin/bash
# This script is highly experimental!!!
# It helps you using this repository on Windows, since it was created on Linux.

sed -i -e 's/venv\/bin\//venv\/Scripts\//' {.vscode/settings.json,setup.sh}

if ! grep -q PyQt5 requirements.txt; then
	echo "PyQt5==5.15.4" >> requirements.txt
fi

sed -i -e 's/python3/python/' {setup.sh,dvc.yaml}
